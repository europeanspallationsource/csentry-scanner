import argparse
import base64
import os
import queue
import logging
import signal
import time
import threading
import requests
import serial
import yaml
import tkinter as tk
from collections import namedtuple
from io import BytesIO
from PIL import Image, ImageTk
from tkinter.scrolledtext import ScrolledText
from tkinter import ttk, VERTICAL, HORIZONTAL, messagebox, N, S, E, W
from csentry import CSEntry


ENDPOINT = {
    'Action': '/inventory/actions',
    'Manufacturer': '/inventory/manufacturers',
    'Model': '/inventory/models',
    'Location': '/inventory/locations',
    'Status': '/inventory/statuses'
}
Code = namedtuple('Code', 'prefix key value')
logger = logging.getLogger(__name__)


def get_config(filename):
    config = {
        'url': 'https://csentry.esss.lu.se',
        'device': '/dev/ttyACM0'
    }
    try:
        with open(filename) as f:
            local = yaml.load(f)
    except Exception as e:
        logger.debug("Couldn't load config from {}. Using default values.".format(filename))
    else:
        config.update(local)
    # Remove invalid keys from config file
    return {key: value for key, value in config.items()
            if key in ('url', 'device', 'token')}


class Scanner(threading.Thread):

    def __init__(self, device, scan_queue):
        super().__init__()
        self.device = device
        self.queue = scan_queue
        self._stop_event = threading.Event()

    def parse(self, line):
        logger.debug('Received: {}'.format(line))
        if line.startswith('CSE:'):
            try:
                code = Code(*line.split(':'))
            except TypeError:
                logger.warning('Discarding invalid code: "{}"'.format(line))
                return
        else:
            # If the line includes "," we assume it's a QRCode
            # like on Juniper switches:
            # EX3400-48T REV B,NX0217180103,7CE2CA64
            # The SN is the second element
            try:
                sn = line.split(',')[1]
            except IndexError:
                # Otherwise we assume the line is the SN
                sn = line
            code = Code('CSE', 'serial_number', sn)
        self.queue.put(code)

    def run(self):
        logger.debug('scanner run')
        ser = serial.Serial(self.device, 115200)
        while not self._stop_event.is_set():
            if ser.in_waiting > 0:
                line = ser.readline()
                self.parse(line.decode('utf-8').strip())
            time.sleep(0.1)

    def stop(self):
        self._stop_event.set()


class QRCodeUi:

    def __init__(self, frame):
        self.frame = frame

    def display_qrcode(self, data, text, column=0, row=0):
        image = Image.open(BytesIO(base64.b64decode(data)))
        image = image.resize((150, 150))
        tk_image = ImageTk.PhotoImage(image)
        label = ttk.Label(self.frame, text=text, image=tk_image, compound='top')
        # Keep a reference to the image to prevent it from being garbage collected
        label.image = tk_image
        label.grid(column=column, row=row)


class AttributeUi(QRCodeUi):

    def __init__(self, frame, qrcodes):
        super().__init__(frame)
        self.qrcodes = qrcodes
        self.images = {}
        self._selected = []
        self.build_tree()

    def build_tree(self):
        self.tree = ttk.Treeview(self.frame, show="tree", selectmode="extended")
        for node in ('Manufacturer', 'Model', 'Location', 'Status'):
            self.tree.insert('', 'end', node, text=node, tags=('node',))
            for elt in self.qrcodes[node]:
                item_id = self.tree.insert(node, 'end', text=elt['name'], tags=('item',))
                # Store the qrcode associated to the item in the self.images dict
                self.images[item_id] = elt['qrcode']
        # We can select multiple items in a treeview using the CTRL key.
        # This is not easy if we need to expand a node.
        # The following events allow to select and unselect items by clicking on them.
        self.tree.tag_bind('node', '<ButtonRelease-1>', self.node_clicked)
        self.tree.tag_bind('item', '<ButtonRelease-1>', self.item_clicked)
        self.tree.grid(column=0, row=0, sticky=(N, S))

    def node_clicked(self, event):
        # We force the selection to remain on the selected items
        # (we don't want the node to be highlighted)
        self.tree.selection_set(self._selected)

    def item_clicked(self, event):
        # Keep track of the selected items
        item = self.tree.identify('item', event.x, event.y)
        if item in self._selected:
            self._selected.remove(item)
        else:
            self._selected.append(item)
        self.tree.selection_set(self._selected)
        self.display_selected_qrcodes()

    def display_selected_qrcodes(self):
        # Destroy the existing QRCodes
        # Skip the first element (the treeview)
        for child in self.frame.winfo_children()[1:]:
            child.destroy()
        for col, item_id in enumerate(self.tree.selection(), start=1):
            name = self.tree.item(item_id, 'text')
            qrcode = self.images[item_id]
            self.display_qrcode(qrcode, name, column=col)


class ActionUi(QRCodeUi):

    def __init__(self, frame, qrcodes):
        super().__init__(frame)
        for (col, action) in enumerate(qrcodes['Action']):
            self.display_qrcode(action['qrcode'], action['name'], column=col)


class ItemUi:

    def __init__(self, frame, qrcodes, scan_queue, csentry):
        self.frame = frame
        self.qrcodes = qrcodes
        self.scan_queue = scan_queue
        self.csentry = csentry
        self.auto_fetch = tk.BooleanVar()
        self.auto_fetch.set(False)
        # If no command is passed to the button, it's not initialized properly
        # (doesn't take the _auto_fetch value into account)
        auto_fetch_check = ttk.Checkbutton(
            self.frame,
            text='Auto fetch',
            variable=self.auto_fetch,
            command=self.auto_check_callback,
            onvalue=True,
            offvalue=False
        )
        auto_fetch_check.grid(column=1, row=0, sticky=W)
        self.ics_id = tk.StringVar()
        ttk.Label(self.frame, text='ICS id:').grid(column=0, row=1, sticky=W)
        ttk.Entry(self.frame, textvariable=self.ics_id, width=25).grid(column=1, row=1, sticky=(W, E))
        self.serial_number = tk.StringVar()
        ttk.Label(self.frame, text='Serial number:').grid(column=0, row=2, sticky=W)
        ttk.Entry(self.frame, textvariable=self.serial_number, width=25).grid(column=1, row=2, sticky=(W, E))
        self.parent = tk.StringVar()
        ttk.Label(self.frame, text='Parent:').grid(column=0, row=3, sticky=W)
        ttk.Entry(self.frame, textvariable=self.parent, width=25).grid(column=1, row=3, sticky=(W, E))
        self.manufacturer = self.combobox('Manufacturer', row=4)
        self.model = self.combobox('Model', row=5)
        self.location = self.combobox('Location', row=6)
        self.status = self.combobox('Status', row=7)
        self.frame.after(100, self.poll_scan_queue)

    def combobox(self, name, row, default=''):
        values = [''] + [elt['name'] for elt in self.qrcodes[name]]
        var = tk.StringVar()
        var.set(default)
        ttk.Label(self.frame, text=name + ':').grid(column=0, row=row, sticky=W)
        combobox = ttk.Combobox(
            self.frame,
            textvariable=var,
            width=25,
            state='readonly',
            values=values
        )
        combobox.grid(column=1, row=row, sticky=(W, E))
        return combobox

    def auto_check_callback(self):
        logger.debug('auto_fetch set to {}'.format(self.auto_fetch.get()))

    def get_data(self):
        data = {key: getattr(self, key).get()
                for key in ('ics_id', 'serial_number', 'parent',
                            'manufacturer', 'model', 'location', 'status')
                if getattr(self, key).get() != ''
                }
        logger.debug(data)
        return data

    def update_entry(self, key, value):
        if value is None:
            value = ''
        try:
            getattr(self, key).set(value)
        except AttributeError:
            logger.error('Unknown entry "{}"'.format(value))

    def register(self):
        data = self.get_data()
        # In general the check should be left to the server.
        # But the API allows to create items with no ICS id to be able
        # to insert existing elements from confluence for example.
        # This should not be allowed for new items.
        if 'ics_id' not in data:
            logger.error("Missing mandatory field 'ics_id'")
        else:
            try:
                item = self.csentry.create_item(**data)
            except Exception as e:
                logger.error(f'Failed to create item: {e}')
            else:
                logger.info(f'item {item["ics_id"]} created')

    def fetch(self):
        ics_id = self.ics_id.get()
        if ics_id != '':
            try:
                item = self.csentry.get_item(ics_id)
            except Exception as e:
                pass
            else:
                for key, value in item.items():
                    if key in ('serial_number', 'parent', 'manufacturer',
                               'model', 'location', 'status'):
                        self.update_entry(key, value)
        else:
            logger.error("Missing mandatory field 'ics_id'")

    def assign_ics_id(self):
        data = self.get_data()
        try:
            ics_id = data.pop('ics_id')
            serial_number = data.pop('serial_number')
        except KeyError:
            logger.error("Missing mandatory field 'ics_id' and/or 'serial_number'")
        else:
            try:
                item = self.csentry.assign_item(ics_id, serial_number)
            except Exception as e:
                logger.error(f'Failed to update item: {e}')
            else:
                logger.info(f'item {item["ics_id"]} updated')

    def update(self):
        data = self.get_data()
        try:
            ics_id = data.pop('ics_id')
        except KeyError:
            logger.error("Missing mandatory field 'ics_id'")
        else:
            # Remove serial number because it can't be updated
            data.pop('serial_number', None)
            try:
                item = self.csentry.update_item(ics_id, data)
            except Exception as e:
                logger.error(f'Failed to update item: {e}')
            else:
                logger.info(f'item {item["ics_id"]} updated')

    def set_as_parent(self):
        ics_id = self.ics_id.get()
        if ics_id != '':
            self.clear_all()
            self.update_entry('parent', ics_id)
        else:
            logger.error("Missing mandatory field 'ics_id'")

    def clear_attributes(self):
        for key in ('manufacturer', 'model', 'location', 'status'):
            getattr(self, key).set('')

    def clear_all(self):
        for key in ('ics_id', 'serial_number', 'parent', 'manufacturer',
                    'model', 'location', 'status'):
            getattr(self, key).set('')

    def process(self, code):
        logger.debug(str(code))
        if code.key == 'action':
            action = code.value.lower().replace(' ', '_')
            logger.debug(f'action: {action}')
            try:
                getattr(self, action)()
            except AttributeError:
                logger.error('Unknown action "{}"'.format(code.value))
        else:
            self.update_entry(code.key, code.value)
            if self.auto_fetch.get() and code.key == 'ics_id' and code.value != '':
                self.fetch()

    def poll_scan_queue(self):
        while True:
            try:
                code = self.scan_queue.get(block=False)
            except queue.Empty:
                break
            else:
                self.process(code)
        self.frame.after(100, self.poll_scan_queue)


class QueueHandler(logging.Handler):
    # This class sends logging records to a queue so that it can be used from different threads
    # The ConsoleUi class polls this queue to display records in a ScrolledText widget
    # Example from Moshe Kaplan: https://gist.github.com/moshekaplan/c425f861de7bbf28ef06
    # (https://stackoverflow.com/questions/13318742/python-logging-to-tkinter-text-widget)
    # is not thread safe!
    # See https://stackoverflow.com/questions/43909849/tkinter-python-crashes-on-new-thread-trying-to-log-on-main-thread

    def __init__(self, log_queue):
        super().__init__()
        self.log_queue = log_queue

    def emit(self, record):
        self.log_queue.put(record)


class ConsoleUi:

    def __init__(self, frame):
        self.frame = frame
        self.log_queue = queue.Queue()
        self.scrolled_text = ScrolledText(frame, state='disabled', height=12)
        self.scrolled_text.grid(row=0, column=0, sticky=(N, S, W, E))
        self.scrolled_text.configure(font='TkFixedFont')
        self.scrolled_text.tag_config('INFO', foreground='black')
        self.scrolled_text.tag_config('DEBUG', foreground='gray')
        self.scrolled_text.tag_config('WARNING', foreground='orange')
        self.scrolled_text.tag_config('ERROR', foreground='red')
        self.scrolled_text.tag_config('CRITICAL', foreground='red', underline=1)
        self.queue_handler = QueueHandler(self.log_queue)
        formatter = logging.Formatter('%(asctime)s: %(message)s')
        self.queue_handler.setFormatter(formatter)
        root_logger = logging.getLogger()
        root_logger.addHandler(self.queue_handler)
        self.frame.after(100, self.poll_log_queue)

    def display(self, record):
        msg = self.queue_handler.format(record)
        self.scrolled_text.configure(state='normal')
        self.scrolled_text.insert(tk.END, msg + '\n', record.levelname)
        self.scrolled_text.configure(state='disabled')
        # Autoscroll to the bottom
        self.scrolled_text.yview(tk.END)

    def poll_log_queue(self):
        while True:
            try:
                record = self.log_queue.get(block=False)
            except queue.Empty:
                break
            else:
                self.display(record)
        self.frame.after(100, self.poll_log_queue)


class MainUi:

    def __init__(self, parent, url, token, device):
        self.parent = parent
        self.csentry = CSEntry(url=url, token=token)
        username = self.csentry.username
        self.qrcodes = self.get_qrcodes()
        self.scan_queue = queue.Queue()
        vertical_pane = ttk.PanedWindow(self.parent, orient=VERTICAL)
        vertical_pane.grid(row=0, column=0, sticky="nsew")
        horizontal_pane = ttk.PanedWindow(vertical_pane, orient=HORIZONTAL)
        vertical_pane.add(horizontal_pane)
        item_frame = ttk.Labelframe(horizontal_pane, text="Item")   # , width=100, height=100)
        item_frame.columnconfigure(1, weight=1)
        horizontal_pane.add(item_frame, weight=1)
        console_frame = ttk.Labelframe(horizontal_pane, text="Console")    # , height=50)
        console_frame.columnconfigure(0, weight=1)
        console_frame.rowconfigure(0, weight=1)
        horizontal_pane.add(console_frame, weight=1)
        action_frame = ttk.Labelframe(vertical_pane, text="Action")  # , width=100, height=100)
        vertical_pane.add(action_frame, weight=1)
        attribute_frame = ttk.Labelframe(vertical_pane, text="Attributes")  # , width=100, height=100)
        attribute_frame.rowconfigure(0, weight=1)
        vertical_pane.add(attribute_frame, weight=1)
        status = ttk.Label(self.parent, text='Logged in as {}'.format(username), relief=tk.SUNKEN, anchor=E)
        vertical_pane.add(status)
        self.console = ConsoleUi(console_frame)
        self.item = ItemUi(item_frame, self.qrcodes, self.scan_queue, self.csentry)
        self.action = ActionUi(action_frame, self.qrcodes)
        self.attribute = AttributeUi(attribute_frame, self.qrcodes)
        self.scanner = Scanner(device, self.scan_queue)
        self.scanner.start()
        self.parent.protocol('WM_DELETE_WINDOW', self.quit)
        self.parent.bind('<Control-q>', self.quit)
        signal.signal(signal.SIGINT, self.quit)

    def get_qrcodes(self):
        return {name: list(self.csentry.get_all(endpoint))
                for (name, endpoint) in ENDPOINT.items()}

    def quit(self, *args):
        print('Quit!')
        self.scanner.stop()
        self.parent.destroy()


class LoginUi:

    def __init__(self, parent, url, device):
        self.parent = parent
        self.url = url
        self.device = device
        self.frame = ttk.Frame(parent, padding="3 3 12 12")
        self.frame.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.columnconfigure(0, weight=1)
        self.frame.columnconfigure(1, weight=1)
        self.frame.rowconfigure(0, weight=1)
        self.frame.rowconfigure(1, weight=1)
        self.frame.rowconfigure(2, weight=1)
        self.username = tk.StringVar()
        ttk.Label(self.frame, text='Username').grid(column=0, row=1, sticky=W)
        username_entry = ttk.Entry(self.frame, textvariable=self.username)
        username_entry.grid(column=1, row=1, sticky=(E, W))
        self.password = tk.StringVar()
        ttk.Label(self.frame, text='Password').grid(column=0, row=2, sticky=W)
        ttk.Entry(self.frame, textvariable=self.password, show='*').grid(column=1, row=2, sticky=(E, W))
        ttk.Button(self.frame, text='Submit', command=self.check_login).grid(column=1, row=3)
        username_entry.focus()
        self.parent.bind('<Return>', self.check_login)

    def check_login(self, *args):
        user = self.username.get()
        try:
            csentry = CSEntry(url=self.url, username=user, password=self.password.get())
        except Exception as e:
            logger.debug(e)
            if '401' in str(e):
                messagebox.showerror(title='Error', message='Invalid username/password')
            else:
                messagebox.showerror(title='Error', message=str(e))
        else:
            # Unbind <Return> from check_login
            self.parent.unbind('<Return>')
            # Display the main UI
            self.frame.destroy()
            # Make sure the main window size is not forced to anything
            self.parent.geometry("")
            MainUi(self.parent, self.url, csentry.token, self.device)


class App:

    def __init__(self, root, url, device, token=None):
        self.root = root
        root.title('CSEntry Scanner')
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        menubar = tk.Menu(root)
        appmenu = tk.Menu(menubar, name='apple')
        menubar.add_cascade(menu=appmenu)
        appmenu.add_command(label='About CSEntry Scanner', command=self.about)
        appmenu.add_separator()
        root.createcommand('tk::mac::ShowPreferences', self.open_preferences)
        root['menu'] = menubar
        if token is None:
            LoginUi(root, url, device)
        else:
            MainUi(root, url, token, device)

    def about(self):
        messagebox.showinfo(title='About CSEntry Scanner', message='Coming soon!')

    def open_preferences(self):
        messagebox.showinfo(title='Preferences', message='Coming soon!')


def main():
    parser = argparse.ArgumentParser(description='CSEntry scanner')
    parser.add_argument('--conf', '-c', default='~/.cse-scan.yml',
                        help='Configuration file [default: "~/.cse-scan.yml"]')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Enable debug logging')
    args = parser.parse_args()
    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(level=level)
    filename = os.path.expanduser(args.conf)
    config = get_config(filename)
    root = tk.Tk()
    app = App(root, **config)
    app.root.mainloop()


if __name__ == '__main__':
    main()
