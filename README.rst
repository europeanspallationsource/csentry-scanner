CSEntry scanner
===============

cse-scan is a simple graphical application to register and update items in the CSEntry web server.
All actions are performed by scanning barcodes or QR codes.

The client has the following features:

    - written in Python, using the tkinter GUI toolkit
    - cross-platform: works on Linux and Mac OS X (should work on Windows)
    - works with scanners that can emulate a regular RS232-based COM port (tested with Honeywell Xenon 1900 Barcode scanner)


.. image:: images/cse-scan.png


Requirements
------------

The application requires Python >= 3.6 and a recent version of Tk (>= 8.5.18).


Usage
-----

::

    $ cse-scan -h
    usage: cse-scan [-h] [--conf CONF] [--debug]

    CSEntry scanner

    optional arguments:
      -h, --help            show this help message and exit
      --conf CONF, -c CONF  Configuration file [default: "~/.cse-scan.yml"]
      --debug, -d           Enable debug logging

The configuration file `~/.cse-scan.yml` is optional. It allows you to overwrite the default
device and url used::

    device: /dev/ttyACM0
    url: https://csentry.esss.lu.se

The default device should work on Linux.
On OS X, the device should be something like `/dev/tty.usbmodem1421`. You can overwrite it by creating
the file `~/.cse-scan.yml`::

    $ cat ~/.cse-scan.yml
    device: /dev/tty.usbmodem1421

To avoid having to login everytime you start the application, you can specify a token in the config file::

    token: <token>


Installation
------------

As the application requires a recent version of Python and Tk, conda_ is recommended
for the installation.

To install from the conda repository in artifactory::

    $ conda create -n cse-scan -c https://artifactory.esss.lu.se/artifactory/ics-conda -c conda-forge cse-scan
    $ conda activate cse-scan

Run::

    $ cse-scan


Development
-----------

To install in "develop mode":

    $ git clone https://gitlab.esss.lu.se/ics-infrastructure/csentry-scanner.git
    $ cd csentry-scanner
    $ conda env create -n cse-scan
    $ conda activate cse-scan
    $ pip install -e .


.. _conda: https://conda.io/miniconda.html
